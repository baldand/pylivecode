pylivecode
----------
(c) Andrew Baldwin 2014

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Requirements
------------
Requires the watchdog framework:

pip install watchdog

Usage
-----

Start with:

python live.py <file_to_watch_and_run>

The given file will be immediately loaded and run, but the
program will not exit.

The file should be almost normal python code.
It should not have any infinite loops - ideally it should run
for a short time and then exit after setting some local state which
will be returned to the calling environment.

Now open <file_to_watch_and_run> in a text editor, modify it
and save the change.

The file will immediately be reloaded and executed.

"import" cannot be used. Instead do this:

modname = use("modfilename")

This will load the file "modfilename.py" and return it, similar to
import.

However, the imported file can be edited, and any changes will trigger
a reload and rerun of the calling script.


