import sys,os,time,Queue

from Watcher import Watcher
from Runner import Runner

if __name__ == "__main__":
    fn = os.path.abspath(sys.argv[1])
    path = os.path.split(fn)[0]
    os.chdir(path)
    w = Watcher(path)
    r = Runner(w)
    run = True
    while 1:
        try:
            if run:
                env = r.run(fn)
            run = w.waitForEvent(timeout=1)
        except KeyboardInterrupt:
            break
        except:
            import traceback
            traceback.print_exc()

    w.stop()
